Tutorial Avasoft Jurnal Module

[[_TOC_]]
# Data Bantu
   ## Currency
   ## Data Account
   ### Setting COA Giro
   ### Setting COA PPN
   ## Data COA
      Menu > Data Bantu > Data Coa > Data Coa
      Sudah di kasih default silahkan edit sesuai kebutuhan
      Note : ada 8 level untuk coa
      Jika diperlukan dapat membuat coa detail , sample 12000000-2
   ## Nomor Pajak
      ### Ambil nomor pajak
         Pengambilan nomor pajak dari dinas
      ### Data Nomor Pajak
         Laporan pemakaian data nomor pajak

# Contact
  Nama
  ppn
  alamat
  dll
  ## detail Currency
     [default](#currency)
  ## Sales & Purchase
     Aktif sebagai customer
     Atkfi sebagai supplier
     <todo> link dari customer / supplier di beli / jual

# Barang
   ## Barang
      Barang Persediaan
      ### Set Barang
      - [Categori Barang](#category-barang)
      - Barang / Jasa
      - Barang : Stok di hitung
      - Jasa : Stok tidak di hitung
      - Asset : <todo> link ke penjelasana ttg aset
      - Dapat Di beli
      - Dapat Di jual
      - Stock Minimal

      Jika Stok kurang dari stok minimal, akan muncul di laporan Stok Mininal (menu > Barang > Stock Minimal)
      - [Categori Satuan](#category-satuan)
      - Unit
         Untuk penghitungan stok , di harapkan satuan nya dengan faktor kali 1
      - [Satuan Beli](#data-satuan)
      - Harga Beli
      - [Satuan Jual](#data-satuan)
      - Harga Jual

      ### [Set coa](#kelompok-barang)
      - Coa Penjualan
      - Coa Retur Penjualan
      - Coa Diskon Penjualan
      - Coa Pembelian
      - Coa Retur Pembelian
      - Coa Penyesuaian Otomatis
      - Coa Penyesuaian Manual
      - Coa Penyesuaian Produksi
      - Coa WIP
   ## Category Barang
      di masing 2 kelompok barang
      di setting , coa untuk kelompok barang tsb
      * Coa Penjualan
      * Coa Retur Penjualan
      * Coa Diskon Penjualan
      * Coa Pembelian
      * Coa Retur Pembelian
      * Coa Penyesuaian Otomatis
      * Coa Penyesuaian Manual
      * Coa Penyesuaian Produksi
      * Coa WIP

      Coa ini akan di pakai di penambahan barang
   ## Satuan
      ### Category Satuan
         Menu > Barang > Satuan > Data Category Satuan

         kategori merupakan pengelompokan satuan yang mempunyai satu kelompok
         contoh :
         * volume
         * panjang
         * berat
      ### Satuan
         Menu > Barang > Satuan > Data Satuan

         data satuan real yg akan di pakai di barang
         contoh :
         * volume
         * liter
         * ml
         * berat
         * ton
         * kg
         * gram
   ## Koreksi Stock
      Jika diperlukan ,untuk adjust koreksi qty / rupiah
      - No Faktur
      - Tanggal Transaksi
      - Nama Barang
      - Qty
      - Satuan
      - Harga Satuan Terkecil
      - Total
   ## Mutasi
      Laporan seluruh mutasi Barang

# Pembelian
# Penjualan
# Kas
# Jurnal
# Setting
   ## Otoritas
   Menu > Setting > User
   Setiap module mempunyai 4 level otoritas
   1. Read
   2. Create
   3. Edit
   4. Delete

   **Untuk Module Data Kas**

   ada otoritas by user allow masing-masing account
   <todo> link to edit otoritas account





1. First ordered list item
2. Another item
   - Unordered sub-list.
3. Actual numbers don't matter, just that it's a number
   1. Ordered sub-list
   2. Next ordered sub-list item
4. And another item.

## urutan gak pake nomor
Unordered lists can:
- use
- minuses

They can also:
* useqq
* asterisks

They can even:
+ use
+ pluses

### pake nomor dan indent
1. First ordered list itemq

   Second paragraph of first item.

1. Another item
#### table
| header 1 | header 2         | header 3                                                                                                                            |
| -------- | ---------------- | ----------------------------------------------------------------------------------------------------------------------------------- |
| cell 1   | cell 2           | cell 3                                                                                                                              |
| cell 4   | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It eventually wraps the text when the cell is too large for the display size. |
| cell 7   |                  | cell 9                                                                                                                              |

##### garis pembagi
Three or more hyphens,
---
asterisks,
***
or underscores
___

###### pembagi pake sama dengan
Alternatively, for H1 and H2, an underline-ish style:
Alt-H1
======
Alt-H2
------

## My first heading

First section content.

## My second heading

Second section content.
"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

## My third heading
"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."


## [Link to File](md2.md)

